#!/usr/bin/env python3

print(10 + 50) # add
print (15 - 50) # subtract
print(5 * 50) # multiply
print( 100 / 50) #divide
print (50 + 30 - 50 * 40 / 50) # PEMDAS
print (10 ** 2) #raising to power. exponents
print (150 % 6) #modulo, get remainder
print( 40 // 6) # divides w/ no remainder