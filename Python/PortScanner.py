#!/usr/bin/env python3

import sys
from datetime import datetime as dt
import socket

if len(sys.argv) == 2:
  goal = socket.gethostbyname(sys.argv[1]) # translates host to ipv4
else:
  print("Invalid number of args")
  print("Syntax: python3 port_scanner.py [ip/hostname]")

print("The Scannning target: " + goal)
print("Time started: " + str(dt.now()))
print('-' * 50)

try:
  for port in range(1,65535):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket.setdefaulttimeout(0.5)
    result = s.connect_ex((goal, port))
    if result == 0:
      print("port {} is open".format(port))
    s.close 
except KeyboardInterrupt:
  print('\nExitting...')
  sys.exit()
except socket.gaierror:
  print("The Hostname could not be resolved")
  sys.exit()
except socket.error:
  print("Could not connect to server")
  sys.exit()