#!/usr/bin/env python3

#print strings
print('Hello, World! use single quotes') #single quotes
print("\n") # new line
print("Hello, World! use double quotes") #double quotes


print (""" This string runs 
multiple lines use triple quote """) # triple quote for multi line 

print("This is "+"a string") # we can also concatenate, this is combining variables in a string